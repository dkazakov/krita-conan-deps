#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
import sys

from conan import ConanFile
from conan import tools
from conan.tools.files import get, patch
from conan.tools.cmake import CMake, CMakeToolchain, cmake_layout

class Eigen2Conan(ConanFile):

    name = "eigen3"
    version = "3.4.0"
    description = "Eigen is a C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms."
    homepage = "https://gitlab.com/libeigen/eigen"
    author = "Dmitry Kazakov <dimula73@gmail.com>"
    license = "MPL2     "
    exports = ["*.patch", "*.diff"]
    settings = "os", "arch", "compiler", "build_type"

    def source(self):
        get(self, 
            "https://gitlab.com/libeigen/eigen/-/archive/3.4.0/eigen-3.4.0.tar.gz", 
            sha256="8586084f71f9bde545ee7fa6d00288b264a2b7ac3607b974e54d13e7162c1c72",
            strip_root=True)
    
    def layout(self):
        cmake_layout(self)
        self.folders.source = "src"
    
    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        patch_files = [
            ]
        
        for file in patch_files:
            if self.settings.os == "Windows":
                self.run("myptch.exe -p1 < {}".format(os.path.join(self.recipe_folder, file)),
                         cwd=self.source_folder)
            else:
                patch(self, patch_file = os.path.join(self.recipe_folder, file), strip = 0, fuzz=True)

        cmake_options = {
            "BUILD_TESTING" : "OF",
        }

        cmake = CMake(self)
        cmake.configure(variables = cmake_options)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
