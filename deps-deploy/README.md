Installing all the deps

Use the following line to install deps into `./root`

```bash
conan install . --deploy=flat_deploy -of root
```