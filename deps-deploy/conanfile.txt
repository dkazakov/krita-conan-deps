[requires]
gsl/2.7.0-ampl
eigen3/3.4.0

[generators]
CMakeDeps
CMakeToolchain
MesonToolchain
