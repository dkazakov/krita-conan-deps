from conan.errors import ConanException
from conan.tools.files import copy
import os


def deploy(graph, output_folder, **kwargs):
    """
    Deploys to output_folder in a flat way
    """
    import shutil

    folder_name = "flat_deploy"
    new_folder = os.path.join(output_folder, folder_name)
    
    if os.path.isdir(new_folder):
        shutil.rmtree(new_folder)

    conanfile = graph.root.conanfile
    conanfile.output.info(f"Flat deployer to {output_folder}")
    for dep in conanfile.dependencies.values():
        conanfile.output.warning("dep: {} {}".format(dep.package_folder, dep.ref.name))
        
        if dep.package_folder is None:
            continue
        shutil.copytree(dep.package_folder, new_folder, symlinks=True, dirs_exist_ok=True, ignore=lambda dir, files: ["conaninfo.txt", "conanmanifest.txt"])
        dep.set_deploy_folder(new_folder)
