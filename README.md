Proof of concepf for the conan build of Krita's dependencies

* eigen3, gsl --- packages for Eigen3 and GSL, correspondingly
* deps-deploy --- a custom deployer for "flattening" the package structure
 
 See more discussion on Phabricator:
 https://phabricator.kde.org/T16346
 