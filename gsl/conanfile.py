#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
import sys

from conan import ConanFile
from conan import tools
from conan.tools.files import get, patch
from conan.tools.cmake import CMake, CMakeToolchain, cmake_layout
#from conans.errors import ConanInvalidConfiguration

class GSLConan(ConanFile):

    name = "gsl"
    version = "2.7.0-ampl"
    description = "GSL, the GNU Scientific Library, a collection of numerical routines for scientific computing."
    homepage = "https://github.com/ampl/gsl"
    author = "Dmitry Kazakov <dimula73@gmail.com>"
    license = "GPL-3.0"
    exports = ["*.patch", "*.diff"]
    settings = "os", "arch", "compiler", "build_type"

    def source(self):
        get(self, 
            "https://github.com/ampl/gsl/archive/8eac3b0b5cffb90004a729f0675011e718f41379.zip", 
            sha256="124bf79bfd2c3cb07df02a2b0e80e13d29a94cab8acae1c2c1e086474381a6c2",
            strip_root=True)
    
    def layout(self):
        cmake_layout(self)
        self.folders.source = "src"
    
    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        patch_files = [
            "gsl-android.patch",
            "gsl-2.3_clang12.patch",
            "windows-no-fPIC.patch"
            ]
        
        self.output.warning("source_folder: {}".format(self.source_folder))
        self.output.warning("export_source_folder: {}".format(self.export_sources_folder))
        self.output.warning("recipe_folder: {}".format(self.recipe_folder))

        self.output.warning("self.folders.source: {}".format(self.folders.source))
        self.output.warning("self.folders.root: {}".format(self.folders.root))
        self.output.warning("self.folders.build: {}".format(self.folders.build))

        self.output.warning("self.cpp.source: {}".format(self.cpp.source))
        self.output.warning("self.cpp.build: {}".format(self.cpp.build))
        self.output.warning("self.cpp.package: {}".format(self.cpp.package))

        self.output.warning("self.layouts.source: {}".format(self.layouts.source))
        self.output.warning("self.layouts.build: {}".format(self.layouts.build))
        self.output.warning("self.layouts.package: {}".format(self.layouts.package))
        
        
        for file in patch_files:
            if self.settings.os == "Windows":
                self.run("myptch.exe -p1 < {}".format(os.path.join(self.recipe_folder, file)),
                         cwd=self.source_folder)
            else:
                patch(self, patch_file = os.path.join(self.recipe_folder, file), strip = 0, fuzz=True)

            

        cmake_options = {
            "GSL_DISABLE_TESTS" : "ON",
            "BUILD_SHARED_LIBS" : "ON",
            "NO_AMPL_BINDINGS" : "ON"
        }

        cmake = CMake(self)
        cmake.configure(variables = cmake_options)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
